<?xml version="1.0" encoding="UTF-8" ?>
<class name="FutariVortex" inherits="Spatial" category="Core" version="3.1">
	<brief_description>
		A Futari Vortex object.
	</brief_description>
	<description>
		This node simulate vortex for futari particles system. 
                Unlike other futari modifier, vortex is a cylinder. Therefore, it must be defined
                by the radius of the basis (range) and a height (parallel to Y axis).
            
                There are 3 forces computed for a vortex:
                [A] centrifuge, perpendicular to Y axis, equivalent to X axis;
                [B] elevation, parallel to Y axis, equivalent to Y axis;
                [C] tangential to basis, equivalent to Z axis.
            
                If you are using GradientTexture to control these forces, RGB cchannels
                will multiply these 3 forces.
            
                This node can be configured in different ways: 
                [A] without gradient texture, it will just apply a tangential acceleration to particles; 
                [B] with only a center gradient that controls the forces relatively to the position of the particles to the basis; the whole cylinder will react in the same way;
                [C] with only a height gradient that controls the strength influence along the height (R channel only);
                [D] with center & height gradients, where center controls the forces relatively to the position of the particles to the basis and height controls the influence of center gradient along the height (R channel only);
                [E] with top, center, bottom and height gradients, where top, center and bottom textures control the forces relatively to the position of the particles to the basis at the top, center and bottom of the vortex, and height gradient controls the influence of these 3 gradients along the height of the cylinder (R: top, G: center, B: bottom).
	</description>
	<tutorials>
	</tutorials>
	<demos>
	</demos>
	<methods>
	</methods>
	<members>
		<member name="enabled" type="bool" setter="set_enabled" getter="get_enabled">
			Enable or disable the vortex in all particles. Warning: this will trigger a shader recompilation, use only when usefull and set strength to 0 to disable temporarily.
		</member>
		<member name="range" type="float" setter="set_range" getter="get_range">
			Maximum distance of vortex's influence, linear decrease by default.
		</member>
		<member name="height" type="float" setter="set_height" getter="get_height">
			Height of the cylinder, Y axis.
		</member>
		<member name="strength" type="float" setter="set_strength" getter="get_strength">
			Multiplier of the forces applied to particles.
		</member>
		<member name="center_tex" type="GradientTexture" setter="set_center_texture" getter="get_center_texture">
			RGB texture to control centrifuge, elevation & tangential forces of the vortex at the center of the vortex.
		</member>
		<member name="top_tex" type="GradientTexture" setter="set_top_texture" getter="get_top_texture">
			RGB texture to control centrifuge, elevation & tangential forces of the vortex at the top of the vortex. If defined, center_tex, bottom_tex & height_tex must also be defined.
		</member>
		<member name="bottom_tex" type="GradientTexture" setter="set_bottom_texture" getter="get_bottom_texture">
			RGB texture to control centrifuge, elevation & tangential forces of the vortex at the bottom of the vortex. If defined, center_tex, top_tex & height_tex must also be defined.
		</member>
		<member name="height_tex" type="GradientTexture" setter="set_height_texture" getter="get_height_texture">
			RGB texture to control centrifuge, elevation & tangential forces of the vortex at the bottom of the vortex if all 3 are defined, only the influence of center gradient if defined or the the strength if no texture is defined.
		</member>
	</members>
	<constants>
	</constants>
</class>
