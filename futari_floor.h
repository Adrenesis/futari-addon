/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_floor.h
 * Author: frankiezafe
 *
 * Created on March 27, 2019, 2:53 PM
 */

#ifndef FUTARI_FLOOR_H
#define FUTARI_FLOOR_H

#include "futari_modifier.h"

#define FUTARI_FLOOR_MINRANGE 0.0

class FutariFloorData : public FutariModifierData {
public:
    
    bool infinite;
    real_t width;
    real_t height;
    Vector3 xaxis;
    Vector3 zaxis;
    real_t viscous;

    bool changed_infinite;
    bool changed_width;
    bool changed_height;
    bool changed_xaxis;
    bool changed_zaxis;
    bool changed_viscous;

    FutariFloorData( ) :
    infinite(false),
    width(1.0),
    height(1.0),
    changed_infinite( false ),
    changed_width( false ),
    changed_height( false ),
    changed_xaxis( false ),
    changed_zaxis( false ),
    changed_viscous( false )
    {
        range = FUTARI_FLOOR_MINRANGE;
    }
    
    void reset( ) {

        FutariModifierData::reset( );
        changed_infinite = false;
        changed_width = false;
        changed_height = false;
        changed_xaxis = false;
        changed_zaxis = false;
        changed_viscous = false;

    }
    
    void operator=( const FutariFloorData& src ) {

        FutariModifierData::operator=( src );
        infinite = src.infinite;
        width = src.width;
        height = src.height;
        xaxis = src.xaxis;
        zaxis = src.zaxis;
        viscous = src.viscous;
        changed_infinite = src.changed_infinite;
        changed_width = src.changed_width;
        changed_height = src.changed_height;
        changed_xaxis = src.changed_xaxis;
        changed_zaxis = src.changed_zaxis;
        changed_viscous = src.changed_viscous;

    }

};

class FutariFloor : public FutariModifier {
    GDCLASS( FutariFloor, FutariModifier )

public:

    FutariFloor( );

    void set_range( real_t r );
    
    void set_infinite( bool b );
    bool is_infinite() const;
    
    void set_width( real_t w );
    real_t get_width() const;
    
    void set_height( real_t h );
    real_t get_height() const;
    
    void set_viscous( real_t h );
    real_t get_viscous() const;

    FutariModifierData* data_ptr( );

protected:

    static void _bind_methods( );
    
    void refresh();

    FutariFloorData _data;
    
    bool infinite;
    real_t width;
    real_t height;
    real_t viscous;
    
    Vector3 xaxis;
    Vector3 yaxis;
    Vector3 zaxis;

private:

};

#endif /* FUTARI_FLOOR_H */

