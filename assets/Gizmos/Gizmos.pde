PVector[] arrow;
PVector[] attr;

float mult;
float radius;
float strength;

void setup() {

  size( 600, 600, P3D );

  arrow = new PVector[5];
  arrow[0] = new PVector(0, -0.3);
  arrow[1] = new PVector(0, -0.8);
  arrow[2] = new PVector(-1, 0);
  arrow[3] = new PVector(0, 0.8);
  arrow[4] = new PVector(0, 0.3);
}

void drawAll() {

  for ( int r = 0; r < 3; ++r ) {

    switch( r ) {
    case 1:
      rotateX( PI / 2 );
      break;
    case 2:
      rotateY( PI / 2 );
      break;
    }

    drawCircle();
  }
}

void drawCircle() {
  for ( int z = 0; z < 4; ++z ) {
    rotateZ( PI / 2 );
    drawPart();
  }
}

void drawPart() {

  float str = strength;
  if (strength > 0) {
    str = min( strength, radius - mult );
    if ( str < 0 ) {
      str = 0;
    }
  }

  // angle of the arrow basis intersection with circle
  float m = mult;
  float proja = 0;
  float arrow_offset = 0;
  float ratio = mult * arrow[4].y / radius;
  if ( ratio > 0.9999 ) {
    ratio = 0.9999;
  }
  proja = asin( ratio );
  if ( strength < 0 && proja > 20 / 180.f * PI ) {
    proja = 20 / 180.f * PI;
    m = abs( sin( proja ) ) * radius / arrow[4].y;
    arrow_offset = 1 - cos(proja);
  }

  stroke( 0, 255, 0, 50 );
  line( 0, 0, cos( proja ) * 1000, sin( proja ) * 1000 );
  stroke( 0, 255, 255 );

  // arrow
  line( 
    radius - arrow_offset * radius, arrow[0].y * m, 
    radius - ( str + arrow_offset * radius ), arrow[0].y * m 
    );
  float multx = 1;
  if ( strength < 0 ) { 
    multx *= -1;
  }
  for ( int i = 0, j = 1; i < 4; ++i, ++j ) {
    line( 
      radius - ( str + arrow_offset * radius ) + arrow[i].x * m * multx, arrow[i].y * m, 
      radius - ( str + arrow_offset * radius ) + arrow[j].x * m * multx, arrow[j].y * m
      );
  }
  line( 
    radius - ( str + arrow_offset * radius ), arrow[4].y * m, 
    radius - arrow_offset * radius, arrow[4].y * m
    );
  // arc
  float a = proja;
  float agap = ( ( PI / 2 ) - proja * 2 ) / 8;
  for ( int i = 0; i < 8; ++i ) {
    line( 
      cos( a ) * radius, sin ( a ) * radius, 
      cos( a + agap ) * radius, sin ( a + agap ) * radius 
      );
    a += agap;
  }
  
}

void draw() {

  mult = 50;
  radius = abs( mouseY - height / 2 );
  strength = mouseX - width / 2;

  background( 30 );

  fill( 200 );
  text( "mult: " + mult, 10, 25 );
  text( "radius: " + radius, 10, 40 );
  text( "strength: " + strength, 10, 55 );

  noFill();
  translate( width / 2, height / 2 );

  //rotateX( 0.2 );
  //rotateY( 0.3 );

  stroke( 255, 0, 0, 150 );
  line( -width, 0, width, 0 );
  stroke( 0, 255, 0, 150 );
  line( 0, -width, 0, width );
  stroke( 0, 0, 255, 150 );
  line( 0, 0, -width, 0, 0, width );

  stroke( 0, 255, 255 );

  //drawAll();
  drawCircle();
  //drawPart();
}